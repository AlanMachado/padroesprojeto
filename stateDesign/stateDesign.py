from abc import ABCMeta, abstractmethod

# State: uma interface que encapsula o comportamento do objeto
# ConcreteState: subclasse que implementa State
# Context: define a interface de interesse do cliente. tambem contribui a manter uma instancia de uma
# subclasse ConcreteState


class State(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def doThis(self):
        pass


class StartState(State):
    def doThis(self):
        print 'TV switching on...'


class StopState(State):
    def doThis(self):
        print 'TV switching off...'


class TVContext(State):
    def __init__(self):
        self.state = None

    def getState(self):
        return self.state

    def setState(self, state):
        self.state = state

    def doThis(self):
        self.state.doThis()