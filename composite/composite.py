from abc import ABCMeta

# composite serve para compor objetos em estruturas de arvores para representar hierarquia
# tratar de maneira uniforme objetos individuais
# criar uma classe base que contem toda a interface necessaria para todos os elementos
# e criar um elemento especial que agrega outros elementos


class ArquivoComponent:
    __metaclass__ = ABCMeta

    nomeDoArquivo = ''

    def printNomeDoArquivo(self):
        print self.nomeDoArquivo

    def getNomeDoArquivo(self):
        return self.nomeDoArquivo

    def adicionar(self, novoArquivo):
        raise Exception('Nao pode inserir arquivos em: ' + self.nomeDoArquivo + ' - Nao e uma pasta')

    def remover(self, nomeDoArquivo):
        raise Exception('Nao pode remover arquivos em: ' + self.nomeDoArquivo + ' - Nao e uma pasta')

    def getArquivo(self, nomeDoArquivo):
        raise Exception('Nao pode pesquisar arquivos em: ' + self.nomeDoArquivo + ' - Nao e uma pasta')


class ArquivoVideo(ArquivoComponent):
    def __init__(self, nomeDoArquivo):
        self.nomeDoArquivo = nomeDoArquivo


class ArquivoComposite(ArquivoComponent):
    def __init__(self, nomeDoArquivo):
        self.arquivos = []
        self.nomeDoArquivo = nomeDoArquivo

    def printNomeDoArquivo(self):
        print self.nomeDoArquivo
        for arquivo in self.arquivos:
            arquivo.printNomeDoArquivo()

    def adicionar(self, novoArquivo):
        self.arquivos.append(novoArquivo)

    def remover(self, nomeDoArquivo):
        for arq in self.arquivos:
            if arq.nomeDoArquivo == nomeDoArquivo:
                self.arquivos.remove(arq)
                return
        raise Exception('Nao existe este arquivo.')

    def getArquivo(self, nomeDoArquivo):
        for arq in self.arquivos:
            if arq.nomeDoArquivo == nomeDoArquivo:
                return arq
        raise Exception('Nao existe este arquivo.')