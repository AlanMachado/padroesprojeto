from composite import *


if __name__ == '__main__':
    minhaPasta = ArquivoComposite('minha pasta/')
    meuVideo = ArquivoVideo('meuvideo.avi')
    meuOutroVideo = ArquivoVideo('blacksails.mkv')

    try:
        meuVideo.adicionar(meuOutroVideo)
    except Exception as e:
        print e.message

    try:
        minhaPasta.adicionar(meuVideo)
        minhaPasta.adicionar(meuOutroVideo)
        minhaPasta.printNomeDoArquivo()
    except Exception as e:
        print e.message

    try:
        print '\nPesquisando arquivos:'
        minhaPasta.getArquivo(meuVideo.getNomeDoArquivo()).printNomeDoArquivo()
        print '\nRemover arquivos:'
        minhaPasta.remover(meuVideo.getNomeDoArquivo())
        minhaPasta.printNomeDoArquivo()
    except Exception as e:
        print e.message