from abc import ABCMeta, abstractmethod
# prover um subistituto para outro objeto, para controlar acesso ao objeto original
# usado como uma camada ou interface para fornecer acesso distribuido
# delega e protege o objeto real de impactos indesejaveis
# proxy ajuda a melhorar a performace de aplicacoes por cachear objetos pesados ou, o acesso frequente a esse objeto
# proxy tambem autoriza o acesso ao Objeto real, e em adicao, esse padrao ajuda na delegacao somente com permissao
# proxy remoto tambem facilita interacao com o server remoto que pode trabalhar como uma con


class You(object):
    def __init__(self):
        print 'You:: Lets buy the Denim shirt'
        self.debitCard = DebitCard()
        self.isPurchased = None

    def make_payment(self):
        self.isPurchased = self.debitCard.do_pay()

    def __del__(self):
        if self.isPurchased:
            print 'You:: Wow! Denim shirt is mine :-)'
        else:
            print 'You:: I should earn more :('


class Payment:
    __metaclass__ = ABCMeta

    @abstractmethod
    def do_pay(self):
        pass


class Bank(Payment):
    def __init__(self):
        self.card = None
        self.account = None

    def __getAccount(self):
        self.account = self.card
        return self.account

    def __hasFunds(self):
        print 'Bank:: Checking if Account', self.__getAccount(), 'has enough funds'
        return True

    def setCard(self, card):
        self.card = card

    def do_pay(self):
        if self.__hasFunds():
            print 'Bank:: Paying the merchant'
            return True
        else:
            print 'Bank:: sorry, not enough funds!'
            return False


class DebitCard(Payment):
    def __init__(self):
        self.bank = Bank()

    def do_pay(self):
        card = input('Proxy:: Punch in Card Number: ')
        self.bank.setCard(card)
        return self.bank.do_pay()