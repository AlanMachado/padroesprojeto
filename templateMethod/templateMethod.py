from abc import ABCMeta, abstractmethod

# quando multiplos algoritmos ou classes implementam logica identica
# A implementacao de algoritmos em subclasses ajuda a reduzir duplicacao de codigo(strategy)
# Multiplos algoritmos podem ser definidos por deixar a subclasse implementar o comportamento por overriding

# define um esqueleto de um algoritmo com operacoes primitivas
# redefine certas operacoes por subclasses sem mudar a estrutura do algoritmo


class MusicaMP3(object):
    def __init__(self, nome, autor, ano, estrela):
        self.nome = nome
        self.autor = autor
        self.ano = ano
        self.estrela = estrela


class OrdenadorTemplate:
    __metaclass__ = ABCMeta

    @abstractmethod
    def isPrimeiro(self, musica1, musica2):
        pass

    def _ordernarMusica(self, lista):
        novaLista = []
        novaLista.extend(lista)

        for i in range(0, len(novaLista)):
            for j in range(i, len(novaLista)):
                if self.isPrimeiro(novaLista[i], novaLista[j]):
                    temp = novaLista[j]
                    novaLista[j] = novaLista[i]
                    novaLista[i] = temp
        return novaLista


class OrdenadorPorAno(OrdenadorTemplate):

    def isPrimeiro(self, musica1, musica2):
        if musica1.ano <= musica2.ano:
            return True
        return False


class OrdenadorPorAutor(OrdenadorTemplate):
    def isPrimeiro(self, musica1, musica2):
        if str.lower(musica1.autor) <= str.lower(musica2.autor):
            return True
        return False


class OrdenadorPorEstrela(OrdenadorTemplate):
    def isPrimeiro(self, musica1, musica2):
        if musica1.estrela <= musica2.estrela:
            return True
        return False


class OrdenadorPorNome(OrdenadorTemplate):
    def isPrimeiro(self, musica1, musica2):
        if str.lower(musica1.nome) <= str.lower(musica2.nome):
            return True
        return False


class PlayList(object):
    def __init__(self, modo):
        self.musicas = []
        self.setModoDeReproducao(modo)

    def setModoDeReproducao(self, modo):
        if modo == 1:
            self.ordenador = OrdenadorPorAno()
        elif modo == 2:
            self.ordenador = OrdenadorPorAutor()
        elif modo == 3:
            self.ordenador = OrdenadorPorEstrela()
        elif modo == 4:
            self.ordenador = OrdenadorPorNome()

    def adicionarMusica(self, musica):
        self.musicas.append(musica)

    def mostrarListaRep(self):
        novalista = self.ordenador._ordernarMusica(self.musicas)

        for musica in novalista:
            print musica.nome + ' - ' + musica.autor + '\n Ano: ' + str(musica.ano) + '\n Estrelas: ' + str(musica.estrela)