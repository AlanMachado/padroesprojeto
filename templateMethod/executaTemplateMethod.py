from templateMethod import *

if __name__ == '__main__':
    minhaPlaylist = PlayList(modo=4)
    minhaPlaylist.adicionarMusica(MusicaMP3(nome='Hallowed be thy name', autor='Iron Maiden', ano=1982, estrela=5))
    minhaPlaylist.adicionarMusica(MusicaMP3(nome='Children of the Damned', autor='Iron Maiden', ano=1982, estrela=4))
    minhaPlaylist.adicionarMusica(MusicaMP3(nome='The Number of the beast', autor='Iron Maiden', ano=1982, estrela=5))
    minhaPlaylist.adicionarMusica(MusicaMP3(nome='Blood Brothers', autor='Iron Maiden', ano=2000, estrela=4))

    minhaPlaylist.mostrarListaRep()

    # minhaPlaylist.setModoDeReproducao(3)
    #
    # print '\n'
    # minhaPlaylist.mostrarListaRep()
