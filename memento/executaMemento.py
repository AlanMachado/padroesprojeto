from memento import *

if __name__ == '__main__':
    texto = Texto()
    texto.escreverTexto("Primeira linha do texto\n")
    texto.escreverTexto("Segunda linha do texto\n")
    texto.escreverTexto("Terceira linha do texto\n")
    texto.mostrarTexto()
    texto.desfazerEscrita()
    texto.mostrarTexto()
    texto.desfazerEscrita()
    texto.mostrarTexto()
    texto.desfazerEscrita()
    texto.mostrarTexto()
    texto.desfazerEscrita()
    texto.mostrarTexto()