# manter estados de objetos
# recuperar estes estados sem ferir o encapsulamento
# sem violar o encapsulamento, capturar e externalizar um estado interno de um objeto,
# de maneira que o objeto possa ser restaurado para esse estado mais tarde


class TextoMemento(object):
    def __init__(self, texto):
        self._estadoTexto = texto

    def getTextoSalvo(self):
        return self._estadoTexto


class CareTaker(object):
    def __init__(self):
        self._estados = []

    def adicionarMemento(self, memento):
        self._estados.append(memento)

    def getUltimoEstadoSalvo(self):
        if len(self._estados) <= 0:
            return TextoMemento('')

        return self._estados.pop()


class Texto(object):
    def __init__(self):
        self._texto = ""
        self._caretaker = CareTaker()

    def escreverTexto(self, novoTexto):
        self._caretaker.adicionarMemento(TextoMemento(novoTexto))
        self._texto += novoTexto

    def desfazerEscrita(self):
        self._texto = self._caretaker.getUltimoEstadoSalvo().getTextoSalvo()

    def mostrarTexto(self):
        print self._texto