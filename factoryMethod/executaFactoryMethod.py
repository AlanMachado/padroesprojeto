from factoryMethod import *

if __name__ == '__main__':
    fiat = FabricaFiat()
    carro = fiat.criarCarro('Palio')
    carro.exibirInfo()

    print

    carro = fiat.criarCarro('Toro')
    carro.exibirInfo()