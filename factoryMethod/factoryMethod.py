from abc import ABCMeta, abstractmethod

# Definir uma interface para criar um objeto, mas deixar as subclasses decidirem que classe instanciar
# ao inves de criar objetos diretamente em uma classe concreta, nos definimos interfaces de criacao de objetos
# e cada subclasse fica responsavel por criar seus objetos
class FabricaDeCarro:
    __metaclass__ = ABCMeta

    @abstractmethod
    def criarCarro(self):
        pass


class Carro:
    __metaclass__ = ABCMeta

    @abstractmethod
    def exibirInfo(self):
        pass


class FabricaFiat(FabricaDeCarro):

    def criarCarro(self, object_type):
        return eval(object_type)()


class Palio(Carro):
    def exibirInfo(self):
        print 'Modelo: Palio\nFabricante: Fiat'


class Toro(Carro):
    def exibirInfo(self):
        print 'Modelo: Toro\nFabricante: Fiat'