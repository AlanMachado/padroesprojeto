from abc import ABCMeta

# dinamicamente agregar responsabilidades adicionais a objetos
class Coquetel:
    __metaclass__ = ABCMeta

    nome = ''
    preco = 0.00

    def getNome(self):
        return self.nome

    def getPreco(self):
        return self.preco


class Cachaca(Coquetel):
    def __init__(self):
        self.nome = 'Cachaca'
        self.preco = 1.5


class CoquetelDecorator(Coquetel):
    def __init__(self, coquetel):
        self.coquetel = coquetel

    def getNome(self):
        return self.coquetel.getNome() + ' + ' + self.nome

    def getPreco(self):
        return str(self.coquetel.getPreco()) + ' + ' + str(self.preco)


class Refrigerante(CoquetelDecorator):
    def __init__(self, coquetel):
        CoquetelDecorator.__init__(self, coquetel)
        self.nome = 'Refrigerante'
        self.preco = 1.0