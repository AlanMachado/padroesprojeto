from abc import ABCMeta, abstractmethod
# encapsula o codigo
# reutilizacao do mesmo
# strategy permite que o codigo varie de acordo com a necessidade dos que o utilizam
class CalculaImposto:
    __metaclass__ = ABCMeta

    @abstractmethod
    def calculaSalarioComImposto(cls, funcionario):
        pass


class CalculaImpostoQuinzeOuDez(CalculaImposto):

    def calculaSalarioComImposto(cls, funcionario):
        if funcionario.getSalarioBase() > 2000:
            return funcionario.getSalarioBase() * 0.85

        return funcionario.getSalarioBase() * 0.9


class CalculaImpostoVinteOuQuinze(CalculaImposto):

    def calculaSalarioComImposto(cls, funcionario):
        if funcionario.getSalarioBase() > 3500:
            return funcionario.getSalarioBase() * 0.8

        return funcionario.getSalarioBase() * 0.85


class Funcionario(object):

    def __init__(self, cargo, salarioBase):
        self._salarioBase = salarioBase
        self.cargo = cargo
        self.__DESENVOLVEDOR = 1
        self.__GERENTE = 2
        self.__DBA = 3

        if self.__DESENVOLVEDOR == cargo:
            self.__estrategiaDeCalculo = CalculaImpostoQuinzeOuDez()
        elif self.__DBA == cargo:
            self.__estrategiaDeCalculo = CalculaImpostoQuinzeOuDez()
        elif self.__GERENTE == cargo:
            self.__estrategiaDeCalculo = CalculaImpostoVinteOuQuinze()
        else:
            raise Exception('Cargo nao encontrado.')

    def calcularSalarioComImposto(self):
        return self.__estrategiaDeCalculo.calculaSalarioComImposto(self)

    def getSalarioBase(self):
        return self._salarioBase
