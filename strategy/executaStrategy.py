from strategy import *


if __name__ == '__main__':
    funcionario1 = Funcionario(1, 2100)
    print funcionario1.calcularSalarioComImposto()

    funcionario2 = Funcionario(1, 1700)
    print funcionario2.calcularSalarioComImposto()

    funcionario3 = Funcionario(2, 4500)
    print funcionario3.calcularSalarioComImposto()