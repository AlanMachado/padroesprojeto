from singleton import *


if __name__ == '__main__':
    s = MeuSingleton()
    print 'Object created %s' % s

    s1 = MeuSingleton()
    print 'Object created %s' % s1

    lazy = LazyInstantiation() ## class initialized, but object not created
    print 'Object created lazy, ', LazyInstantiation.getInstance()  ## object gets created here
    lazy1 = LazyInstantiation() ## instance already created

    b = Borg()
    b2 = Borg()
    b.x = 4

    print 'Borg Object b: ', b
    print 'Borg Object b2: ', b2

    print 'State b: ', b.__dict__
    print 'State b2: ', b2.__dict__

    print '\n'
    print '\n'

    logs = Logger().Log()
    logs1 = Logger().Log()
    print logs, logs1

    hc1 = HealthCheck()
    hc2 = HealthCheck()
    hc1.addServer()
    hc2.changeServer()

    for i in range(4):
        print 'Checking ', hc1._servers[i]




