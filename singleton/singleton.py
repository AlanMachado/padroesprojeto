# assegurar que uma e somente uma instancia do objeto sera criada durante a execucao
# prover um ponto de acesso a este objeto de forma global ao programa
# controlar os acessos ao recurso compartilhado


class MeuSingleton(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(MeuSingleton, cls).__new__(cls)

        return cls.instance


class LazyInstantiation(object):
    __instance = None

    def __init__(self):
        if not LazyInstantiation.__instance:
            print '__init__ method called...'
        else:
            print 'Instance already created: %s' % self.getInstance()

    @classmethod
    def getInstance(cls):
        if not cls.__instance:
            cls.__instance = LazyInstantiation()
        return cls.__instance

#monostate pattern Alex Martelli
class Borg(object):
    __shared_state = {'1':'2'}

    def __init__(self):
        self.x = 1
        self.__dict__ = self.__shared_state
        pass

#frequente
class MetaSingleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Logger:
    __metaclass__ = MetaSingleton
    logger = None

    def Log(self):
        if self.logger is None:
            self.logger = 'Meu Log'
        return hex(id(self.logger))

#more common
class HealthCheck(object):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if HealthCheck._instance is None:
            HealthCheck._instance = super(HealthCheck, cls).__new__(cls, *args, **kwargs)
        return HealthCheck._instance

    def __init__(self):
        self._servers = []

    def addServer(self):
        self._servers.append('Server 1')
        self._servers.append('Server 2')
        self._servers.append('Server 3')
        self._servers.append('Server 4')

    def changeServer(self):
        self._servers.pop()
        self._servers.append('Server 5')