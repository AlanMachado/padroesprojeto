from abc import ABCMeta, abstractmethod
from collections import deque
# Um objeto e usado para encapsular toda a informacao necessaria para executar uma acao ou acionar um evento mais tarde
# Command, Receiver, Invoker and Client
# Encapsular uma requisicao como um objeto
# Permitir parametrizacao de clientes com diferentes requests
# Permitir salvar as requisicoes em uma queue
# Prover um callback O.O.


class Order: #command
    __metaclass__ = ABCMeta

    @abstractmethod
    def execute(self):
        pass


class BuyStockOrder(Order): #concreteCommand
    def __init__(self, stock):
        self.stock = stock

    def execute(self):
        self.stock.buy()


class SellStockOrder(Order): #concreteCommand
    def __init__(self, stock):
        self.stock = stock

    def execute(self):
        self.stock.sell()


class StockTrade: #Receiver
    def buy(self):
        print 'You will buy stocks'

    def sell(self):
        print 'You will sell stocks'


class Agent(object): #Invoker
    def __init__(self):
        self.__orderQueue = deque()

    def placeOrder(self, order):
        self.__orderQueue.append(order)
        order.execute()