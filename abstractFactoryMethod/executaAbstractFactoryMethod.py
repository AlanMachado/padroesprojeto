from abstractFactoryMethod import *

if __name__ == '__main__':
    fabrica = FabricaFiat()
    sedan = fabrica.criarCarroSedan()
    popular = fabrica.criarCarroPopular()
    sedan.exibirInfoSedan()
    print '\n'
    popular.exibirInfoPopular()