from abc import ABCMeta, abstractmethod

class FabricaDeCarro:
    __metaclass__ = ABCMeta

    @abstractmethod
    def criarCarroSedan(self):
        pass

    @abstractmethod
    def criarCarroPopular(self):
        pass


class CarroPopular:
    __metaclass__ = ABCMeta

    @abstractmethod
    def exibirInfoPopular(self):
        pass


class CarroSedan:
    __metaclass__ = ABCMeta
    @abstractmethod
    def exibirInfoSedan(self):
        pass


class FabricaFiat(FabricaDeCarro):

    def criarCarroPopular(self):
        return Palio()

    def criarCarroSedan(self):
        return Siena()


class Palio(CarroPopular):
    def exibirInfoPopular(self):
        print 'Modelo: Palio\nFabricante: Fiat\nCategoria: Popular'


class Siena(CarroSedan):
    def exibirInfoSedan(self):
        print 'Modelo: Siena\nFabrica: Fiat\nCategoria: Sedan'