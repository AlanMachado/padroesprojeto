from observer import *


if __name__ == '__main__':
    news_publisher = NewsPublisher()

    for Subscribers in [SMSSubscriber, EmailSubscriber, AnyOtherSubscriber]:
        Subscribers(news_publisher)

    print '\nSubscribers:', news_publisher.subscribers()

    news_publisher.addNews('Hello World!!!')
    news_publisher.notifySubscribers()

    print '\nDetached:', type(news_publisher.detach()).__name__
    print '\nSubscribers:', news_publisher.subscribers()

    news_publisher.addNews('My second news!!')
    news_publisher.notifySubscribers()