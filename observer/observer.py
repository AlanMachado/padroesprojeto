from abc import ABCMeta, abstractmethod

# define uma dependencia one-to-many entre objetos, assim qualquer mudanca em um objeto sera notificada para os outros
# objetos dependentes automaticamente
# encapsula o core do objeto principal
# existem 2 tipos de Observer, push e pull mode
# no pull os Observers sao notificados que uma mudanca existe, e entao os Observers buscam a informacao
# no push o Publisher ou Subject e dominante, ou seja ele envia as informacoes quando disponiveis


class NewsPublisher(object):
    def __init__(self):
        self.__subscribers = []
        self.__latestNews = None

    def attach(self, subscriber):
        self.__subscribers.append(subscriber)

    def detach(self):
        return self.__subscribers.pop()

    def subscribers(self):
        return [type(x).__name__ for x in self.__subscribers]

    def notifySubscribers(self):
        for sub in self.__subscribers:
            sub.update()

    def addNews(self, news):
        self.__latestNews = news

    def getNews(self):
        return 'Got News:', self.__latestNews


class Subscriber(object): # define uma interface para observers
    __metaclass__ = ABCMeta

    @abstractmethod
    def update(self):
        pass


class SMSSubscriber(Subscriber):
    def __init__(self, publisher):
        self.publisher = publisher
        self.publisher.attach(self)

    def update(self):
        print type(self).__name__, self.publisher.getNews()


class EmailSubscriber(Subscriber):
    def __init__(self, publisher):
        self.publisher = publisher
        self.publisher.attach(self)

    def update(self):
        print type(self).__name__, self.publisher.getNews()


class AnyOtherSubscriber(Subscriber):
    def __init__(self, publisher):
        self.publisher = publisher
        self.publisher.attach(self)

    def update(self):
        print type(self).__name__, self.publisher.getNews()